package com.example.demo.controller;

import com.example.demo.pojo.Book;
import org.springframework.web.bind.annotation.*;

@RestController
public class MyController {

    @GetMapping("/sum")
    Integer sum(@RequestParam("a") Integer one,
                @RequestParam("b") Integer two) {
        return one + two;
    }

    @GetMapping("/factorial")
    Long factorial(@RequestParam("a") Integer a) {
        Long kq = 1l;
        if (a == 0) {
            kq = 0l;
            return kq;
        }
        for (int i = a; i > 0; i--) {
            kq = kq * i;
        }
        return kq;
    }

    @PostMapping("/save-book")
    String saveBook(@RequestBody Book book) {
        return book.getName();
    }
}
